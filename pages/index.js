import Link from 'next/link';

const ENV_TYPE = process.browser ? 'client' : 'server';

function Index({ env }) {
  console.log(`(${ENV_TYPE}) APP_ENV: "${process.env.APP_ENV}"!`);
  console.log(`(${ENV_TYPE}) GIT_COMMIT: "${process.env.GIT_COMMIT}"!`);
  console.log(`(${ENV_TYPE}) env:`, env);
  return (
    <main>
      <section>
        <h2>Hello</h2>
        <ul>
          <li>APP_ENV: "{process.env.APP_ENV}"</li>
          <li>GIT_COMMIT: "{process.env.GIT_COMMIT}"</li>
        </ul>
        <Link href="/test" passHref>
          <a>Go to test</a>
        </Link>
      </section>
    </main>
  );
}

Index.getInitialProps = ({}) => {
  console.log(`(${ENV_TYPE}) APP_ENV: "${process.env.APP_ENV}"!`);
  console.log(`(${ENV_TYPE}) GIT_COMMIT: "${process.env.GIT_COMMIT}"!`);

  return {
    env: {
      APP_ENV: process.env.APP_ENV,
      GIT_COMMIT: process.env.GIT_COMMIT,
    },
  };
};

export default Index;
