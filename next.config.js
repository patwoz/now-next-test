module.exports = {
  target: 'serverless',
  env: {
    APP_ENV: process.env.APP_ENV,
    GIT_COMMIT: process.env.GIT_COMMIT,
  },
};
